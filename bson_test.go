package mongoutils_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/mongoutils"
)

type dummy struct {
	Array   []string `bson:",omitempty"`
	Pointer *string
	String  string
	Number  int
	Ignored int `bson:"-"`
}

func TestIsEmptyBSON(t *testing.T) {
	t.Run("nil", func(t *testing.T) {
		assert.True(t, mongoutils.IsEmptyBSON(nil))
	})

	generator := func(modify func(*dummy), exp bool) func(*testing.T) {
		return func(t *testing.T) {
			var d dummy
			modify(&d)

			val := mongoutils.IsEmptyBSON(&d)
			if exp {
				assert.True(t, val)
			} else {
				assert.False(t, val)
			}
		}
	}

	t.Run("Empty", generator(func(d *dummy) {}, true))
	t.Run("Ignored", generator(func(d *dummy) { d.Ignored = 1 }, true))
	t.Run("Array", generator(func(d *dummy) { d.Array = []string{"hello"} }, false))
	// we can't deal with empty slices or maps
	t.Run("EmptyArray", generator(func(d *dummy) { d.Array = []string{} }, false))
}

// Empty object is the slowest case
func BenchmarkIsEmptyBSON(b *testing.B) {
	var d dummy
	for i := 0; i < b.N; i++ {
		_ = mongoutils.IsEmptyBSON(&d)
	}
}
