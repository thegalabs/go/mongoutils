module gitlab.com/thegalabs/go/mongoutils

go 1.16

require (
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.1
	go.mongodb.org/mongo-driver v1.9.1
)
