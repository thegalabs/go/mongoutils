//nolint:gofmt,goimports,nolintlint
//+build !mongo_debug

package mongoutils

import "go.mongodb.org/mongo-driver/mongo/options"

func noOptions(o *options.ClientOptions) *options.ClientOptions {
	return o
}
