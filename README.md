# Mongo Utils

A set of utils to deal with the mongo client.

Features include:
- handling migrations
- a transaction function wrapper that compiles for with and without replica set
