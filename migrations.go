package mongoutils

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Migration reprensents a database migration
type Migration interface {
	ID() string
	Migrate(context.Context, *mongo.Database) error
}

// MigrationData stores informations about executed migrations
type MigrationData struct {
	MigrationID string
	Date        time.Time
}

// LastMigrationID returns the ID of the last executed migration
func LastMigrationID(ctx context.Context, coll *mongo.Collection, migrations ...Migration) (string, error) {
	criteria := bson.M{}
	if len(migrations) != 0 {
		ids := make([]string, len(migrations))
		for i, m := range migrations {
			ids[i] = m.ID()
		}

		criteria = bson.M{"migrationid": bson.M{"$in": ids}}
	}

	opts := (&options.FindOptions{Sort: bson.M{"_id": -1}}).SetLimit(1)
	cursor, err := coll.Find(ctx, criteria, opts)
	if err != nil {
		return "", err
	}

	if !cursor.Next(ctx) {
		return "", nil
	}

	var mig MigrationData
	if err = cursor.Decode(&mig); err != nil {
		return "", err
	}

	return mig.MigrationID, nil
}

// MigrationCollection is the name of the collection used for migrations
const MigrationCollection = "migrations"

// RunMigrations execute required migrations for the db
func RunMigrations(ctx context.Context, db *mongo.Database, migrations ...Migration) error {
	coll := db.Collection(MigrationCollection)

	lastID, err := LastMigrationID(ctx, coll, migrations...)
	if err != nil {
		return err
	}

	migrations = MigrationsToRun(lastID, migrations...)

	for _, m := range migrations {
		if err = executeMigration(ctx, m, db, coll); err != nil {
			return err
		}
	}
	return nil
}

// MigrationsToRun finds which migration should be ran
// Current implementation is to find the id of the last migration in the array, and execute
// all subsequent ones. This is pretty naive...
func MigrationsToRun(lastID string, migrations ...Migration) []Migration {
	if lastID == "" {
		return migrations
	}
	index := 0
	for i, m := range migrations {
		if m.ID() == lastID {
			index = i + 1
			break
		}
	}

	if index >= len(migrations) {
		return nil
	}
	return migrations[index:]
}

func executeMigration(ctx context.Context, mig Migration, db *mongo.Database, coll *mongo.Collection) error {
	if err := mig.Migrate(ctx, db); err != nil {
		return err
	}

	_, err := coll.InsertOne(ctx, &MigrationData{MigrationID: mig.ID(), Date: time.Now()})

	log.Info().Msgf("Done running migration with id '%s'", mig.ID())
	return err
}
