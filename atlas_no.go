//go:build no_atlas
// +build no_atlas

package mongoutils

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type dummyContext struct {
	context.Context
	mongo.Session
}

// Transaction just executes each transactions
func Transaction(
	ctx context.Context,
	client *mongo.Client,
	functions ...TransactionFunction) (interface{}, error) {
	return transactionCallback(functions...)(dummyContext{Context: ctx})
}
