package mongoutils

import (
	"reflect"
)

// IsEmptyBSON returns true if marshaling to bson would return an empty value
func IsEmptyBSON(t interface{}) bool {
	if t == nil {
		return true
	}

	val := reflect.ValueOf(t).Elem()
	typ := reflect.TypeOf(t).Elem()
	if typ.Kind() != reflect.Struct {
		return val.IsZero()
	}

	for i := 0; i < typ.NumField(); i++ {
		tag := typ.Field(i).Tag.Get("bson")
		if tag != "-" && !val.Field(i).IsZero() {
			return false
		}
	}
	return true
}
