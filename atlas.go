//nolint:gofmt,goimports,nolintlint
//+build !no_atlas

package mongoutils

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

// Transaction executes a transaction on a mongo replica set
func Transaction(
	ctx context.Context,
	client *mongo.Client,
	functions ...TransactionFunction) (interface{}, error) {
	session, err := client.StartSession()
	if err != nil {
		return nil, err
	}
	defer session.EndSession(ctx)

	return session.WithTransaction(ctx,
		func(ctx mongo.SessionContext) (interface{}, error) {
			return transactionCallback(functions...)(ctx)
		},
	)
}
