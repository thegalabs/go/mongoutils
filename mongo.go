// Package mongoutils helps with setting up the mongo db
package mongoutils

import (
	"context"
	"errors"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// NewWithOptions allows modifying the toptions
func NewWithOptions(
	ctx context.Context,
	uri,
	name string,
	opts func(*options.ClientOptions) *options.ClientOptions,
	migrations ...Migration) (client *mongo.Client, db *mongo.Database, err error) {
	client, err = mongo.Connect(ctx, opts(options.Client().ApplyURI(uri)))
	if err != nil {
		return
	}

	// Waiting for client to be connected
	if err = client.Ping(ctx, readpref.Primary()); err != nil {
		return
	}

	db = client.Database(name)
	if len(migrations) > 0 {
		err = RunMigrations(ctx, db, migrations...)
	}

	return
}

// New connects a mongo client using provided values
func New(ctx context.Context, uri, name string, migrations ...Migration) (client *mongo.Client, db *mongo.Database, err error) {
	return NewWithOptions(ctx, uri, name, noOptions, migrations...)
}

// NewFromEnv connects a mongo client using the env var MONGO_URI and MONGO_DATABASE
func NewFromEnv(ctx context.Context, migrations ...Migration) (*mongo.Client, *mongo.Database, error) {
	uri, ok := os.LookupEnv("MONGO_URI")
	if !ok {
		return nil, nil, errors.New("missing MONGO_URI env variable")
	}

	name, ok := os.LookupEnv("MONGO_DATABASE")
	if !ok {
		return nil, nil, errors.New("missing MONGO_DATABASE env variable")
	}
	return New(ctx, uri, name, migrations...)
}

// ErrDuplicate is the mongo code returned when there is a duplicate key
const ErrDuplicate = 11000

// ErrInvalidDocument is returned when the document is invalid
const ErrInvalidDocument = 121

// IsWriteError checks for an error code within the list of write errors
func IsWriteError(err error, code int) bool {
	var count int
	var fn func(int) mongo.WriteError
	switch e := err.(type) {
	case mongo.BulkWriteException:
		count = len(e.WriteErrors)
		fn = func(i int) mongo.WriteError { return e.WriteErrors[i].WriteError }
	case mongo.WriteException:
		count = len(e.WriteErrors)
		fn = func(i int) mongo.WriteError { return e.WriteErrors[i] }
	default:
		return false
	}
	for i := 0; i < count; i++ {
		if fn(i).Code == code {
			return true
		}
	}
	return false
}
