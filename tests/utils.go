// Package tests provides utils function for testing
package tests

import (
	"context"
	"os"
	"sync"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/thegalabs/go/mongoutils"
)

var dbMutext sync.Mutex

// OpenTestDB opens a database
// Since tests are ran in parallel it also locks it.
// it should be followed by ReleaseTestDB
func OpenTestDB(
	t *testing.T,
	name string,
	migrate func(*mongo.Database, context.Context) error) (db *mongo.Database, close func(context.Context)) {
	dbMutext.Lock()

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)

	mc, db, err := mongoutils.New(ctx, os.Getenv("MONGO_URI"), name)
	close = func(ctx context.Context) {
		_ = mc.Disconnect(ctx)
		cancel()
		dbMutext.Unlock()
	}

	if err != nil {
		close(ctx)
		t.Fatal(err)
		return
	}

	if migrate != nil {
		err = migrate(db, ctx)
		if err != nil {
			close(ctx)
			t.Fatal(err)
			return
		}
	}
	return
}

// ReleaseTestDB releases the lock
func ReleaseTestDB() {
	dbMutext.Unlock()
}
