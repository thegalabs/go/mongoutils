package mongoutils_test

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/thegalabs/go/mongoutils"
)

func testDB(t *testing.T) (*mongo.Database, func()) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)

	mc, db, err := mongoutils.New(ctx, os.Getenv("MONGO_URI"), os.Getenv("MONGO_DATABASE"))
	assert.Nil(t, err)
	assert.Nil(t, db.Collection("migrations").Drop(ctx))

	return db, func() {
		_ = mc.Disconnect(ctx)
		cancel()
	}
}

type dummyMigration int

func (d dummyMigration) ID() string {
	return strconv.Itoa(int(d))
}

func (dummyMigration) Migrate(context.Context, *mongo.Database) error {
	return nil
}

func TestLastMigrationID(t *testing.T) {
	db, cancel := testDB(t)
	defer cancel()

	coll := db.Collection("migrations")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	migrations := make([]mongoutils.Migration, 10)
	now := time.Now()
	for i := range migrations {
		_, err := coll.InsertOne(ctx, &mongoutils.MigrationData{
			MigrationID: fmt.Sprintf("%d", i),
			Date:        now.Add(time.Duration(i) * time.Second),
		})
		assert.Nil(t, err)
		migrations[i] = dummyMigration(i)
	}

	generator := func(exp string, modify func([]mongoutils.Migration) []mongoutils.Migration) func(*testing.T) {
		return func(t *testing.T) {
			id, err := mongoutils.LastMigrationID(ctx, coll, modify(migrations)...)
			assert.Nil(t, err)
			assert.Equal(t, exp, id)
		}
	}

	t.Run("all", generator("9", func(m []mongoutils.Migration) []mongoutils.Migration { return m }))
	t.Run("only last", generator("9", func(m []mongoutils.Migration) []mongoutils.Migration {
		return m[9:]
	}))
	t.Run("empty", generator("9", func(m []mongoutils.Migration) []mongoutils.Migration {
		return []mongoutils.Migration{}
	}))
	t.Run("partial", generator("3", func(m []mongoutils.Migration) []mongoutils.Migration {
		return m[:4]
	}))
}
