package mongoutils

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

// TransactionFunction is a function that is part of a transaction
type TransactionFunction = func(ctx context.Context, data interface{}) (interface{}, error)

// utils to be used in transactions
func transactionCallback(functions ...TransactionFunction) func(mongo.SessionContext) (interface{}, error) {
	return func(ctx mongo.SessionContext) (out interface{}, err error) {
		for _, f := range functions[:len(functions)-1] {
			if out, err = f(ctx, out); err != nil {
				return
			}
		}
		return functions[len(functions)-1](ctx, out)
	}
}
