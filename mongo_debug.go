//+build mongo_debug

package mongoutils

import (
	"context"

	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/event"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func noOptions(o *options.ClientOptions) *options.ClientOptions {
	return o.SetMonitor(&event.CommandMonitor{
		Started: func(_ context.Context, evt *event.CommandStartedEvent) {
			log.Debug().Msg(evt.Command.String())
		},
	})
}
